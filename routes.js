import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import countMoviment from './src/countMovement';
import countImage from './src/countImage';
import HomeScreen from './src/homeScreen';


import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons'

const Tab = createBottomTabNavigator()

function Routes() {
  return (
    <Tab.Navigator
            screenOptions={{
                tabBarActiveTintColor: 'gray',
                tabBarInactiveTintColor: '#f1f1f1',
                tabBarActiveBackgroundColor:'black',
                tabBarInactiveBackgroundColor:'black',
                
                tabBarShowLabel: false,
                tabBarStyle:{
                    position: 'absolute',
                    backgroundColor: 'white',
                    borderTopWidth: 0.15,
                }
            }}
        >
            <Tab.Screen
                name="Página Home"
                component={HomeScreen}
                options={{
                    headerShown: true,
                    tabBarIcon: ({color, size, focused}) =>{
                        if(focused){
                            return <Ionicons name="duplicate" size={size} color={color}/>
                        }

                        return <Ionicons name="duplicate-outline" size={size} color={color}/>
                    }
                }}
            />
            <Tab.Screen
                name="Página Contadora"
                component={countMoviment}
                options={{
                    headerShown: true,
                    tabBarIcon: ({color, size, focused}) =>{
                        if(focused){
                            return <Ionicons name="duplicate" size={size} color={color}/>
                        }

                        return <Ionicons name="duplicate-outline" size={size} color={color}/>
                    }
                }}
            />
            <Tab.Screen
                name="Página Imagem"
                component={countImage}
                options={{
                    headerShown: true,
                    tabBarIcon: ({color, size, focused}) =>{
                        if(focused){
                            return <Ionicons name="duplicate" size={size} color={color}/>
                        }

                        return <Ionicons name="duplicate-outline" size={size} color={color}/>
                    }
                }}
            />
            
            
          
                        
        </Tab.Navigator>
    );
}

export default Routes