import React from 'react';
import { useRef, useEffect, useState } from 'react';
import { View, Text, PanResponder, Dimensions, Button, StyleSheet} from 'react-native';
import { useNavigation } from '@react-navigation/native';

const HomeScreen = () => {

    const navegar = useNavigation()
    navegar.setOptions({
        headerTitle: 'Usuários',
        headerStyle: 
        { backgroundColor: 'black',},
        headerTintColor: '#fff',
      });


    const pagImages = () => {
        navegar.navigate("Página Imagem")
    }
    const pagMoviment = () => {
        navegar.navigate("Página Contadora")
    }


return(

  <View style={styles.container}>
    <Button title='Ver página de movimento da imagem' onPress={pagImages}></Button>
    <Button title='Ver página de movimento' onPress={pagMoviment}></Button>

  </View>
);
};

const styles = StyleSheet.create({
  container:{
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#FFF'
  },
 


})

export default HomeScreen;