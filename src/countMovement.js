import React from 'react';
import { useRef, useEffect, useState } from 'react';
import { View, Text, PanResponder, Dimensions, Button, StyleSheet} from 'react-native';

const countMoviment = () => {
 const [count, setCount] = useState(0);
 const screenHeight = Dimensions.get('window').height
 const gestureThreshold = screenHeight * 0.25

 const panResponder = React.useRef(
    
  PanResponder.create({

      onStartShouldSetPanResponder: () => true,
      onPanResponderMove:(event, gestureState) => {},
      onPanResponderRelease:(event, gestureState) => {
        if(gestureState.dy < -gestureThreshold){
            setCount((prevCount) => prevCount +1 );
            
          }
      }

  })

).current;


return(

  <View style={styles.container}{...panResponder.panHandlers}>
      <Text style={styles.text}>Deslize a tela e conte mais um {count}</Text>

  </View>
);
};

const styles = StyleSheet.create({
  container:{
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#FFF'
  },
  text:{
      fontSize:20,
      fontWeight:'bold'

  }


})

export default countMoviment;