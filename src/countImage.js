import React, { useRef, useEffect, useState } from 'react';
import { View, Text, PanResponder, Dimensions, Image, StyleSheet } from 'react-native';

const countImage = () => {
  const screenHeight = Dimensions.get('window').height;
  const gestureThreshold = screenHeight * 0.25;
  const [contaImage, setContaImage] = useState(0);

  const images = [
    { id: 1, image: require('../assets/mesicareca.jpg') },
    { id: 2, image: require('../assets/bola.png') },
    { id: 3, image: require('../assets/cr7careca.png') },
  ];

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dx < -gestureThreshold)  /* esquerda  <<>*/ {

          setContaImage((prevCount) => (prevCount + 1) % images.length);

        } 

        else if (gestureState.dx > gestureThreshold)  /* direita >>< */ {
          setContaImage((prevCount) => {

            if (prevCount === 0) {
              return images.length - 1;
            }

            return prevCount - 1;

          });
        }
      },
    }),
  ).current;

  return (
    <View style={styles.container} {...panResponder.panHandlers}>
        <Text style={styles.text}>Como ser bola profissionalmente igual eu</Text>
      {images[contaImage] && ( <Image source={images[contaImage].image} style={styles.image} /> )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF',
  },
  image: {
    width: 300,
    height: 300,
    resizeMode: 'contain',
  },
  text:{
    fontSize: 28,
    fontWeight: 'bold',
    color: 'red',
    marginBottom: 10,
    marginTop: '-10'
  }
});

export default countImage;
